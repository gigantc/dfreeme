    <?php include("head.php"); ?>
    <body>

    <div class="container">
        <div class="main page">
            

            <section class="dashboard">
                <div class="clock">
                    <canvas id="circle-seconds" width="350" height="350"></canvas>
                    <canvas id="circle-minutes" width="350" height="350"></canvas>
                    <canvas id="circle-hours" width="350" height="350"></canvas>
                </div>
                <div class="time"></div>
                <div class="date"></div>
            </section>

            <section class="feeds"></section>

            <!-- <section class="links">
                <a href="https://football.fantasysports.yahoo.com/" target="blank">FF</a>
            </section> -->

        </div>


        <ul class="menu">
            <li class="btn menu-time">Time</li>
            <li class="btn menu-feeds">Feeds</li>
            <!-- <li class="btn menu-links">Links</li> -->
        </ul>
        <div class="refresh">Refresh</div>


    </div>
    
    <?php include("scripts.php"); ?>
    </body>
</html>
