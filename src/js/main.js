/*

  dFree Boilerplate

  Site Development by Kitchen Sink Studios, Inc
  Developer: Daniel Freeman [@dFree]

*/

$(document).ready(function() { 


// ----------------------------------------
// GLOBALS --------------------------------
// ----------------------------------------
var isOpen = 0;


// ----------------------------------------
// TIME FUNCTIONS -------------------------
// ----------------------------------------
function getCurrentTime() {
  var hours = new Date().getHours() % 12;
	if (hours == 0) {
	    hours += 12;
	}
    var now = new Date(),
		hours = hours > 9 ? hours : '0' + hours,
    minutes = now.getMinutes(),
		minutes = minutes > 9 ? minutes : '0' + minutes,
		seconds = now.getSeconds(),
		seconds = seconds > 9 ? seconds : '0' + seconds,
    now = hours +' '+minutes+' '+seconds;
    $('.time').html(now);
    drawSeconds(seconds);
    drawMinutes(minutes);
    drawHours(hours);
    getCurrentDate();
    setTimeout(getCurrentTime, 1000);
}
getCurrentTime();

function getCurrentDate(){
	var months = [ "January", "February", "March", "April", "May", "June", 
               "July", "August", "September", "October", "November", "December" ];
	var date = new Date();
	var month = date.getMonth();
	var wordsMonth = months[month];
	var day = date.getDate();
	var currentDate = 
		wordsMonth + "  " +
		(day<10 ? '0' : '') + day + ' ' +
		date.getFullYear();

	$('.date').html(currentDate);
}

// CLOCK CIRCLES -----------------------
function drawSeconds(sec){
  var seconds = (2/60)*sec;
  var canvas = document.getElementById('circle-seconds');
  var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.beginPath();
    context.arc(175, 175, 170, 0, seconds*Math.PI, false);
    context.lineWidth = 3;
    context.strokeStyle = '#1b516a';
    context.stroke();
}
function drawMinutes(min){
  var minutes = (2/60)*min;
  var canvas = document.getElementById('circle-minutes');
  var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.beginPath();
    context.arc(175, 175, 150, 0, minutes*Math.PI, false);
    context.lineWidth = 20;
    context.strokeStyle = '#1b516a';
    context.stroke();
}

function drawHours(hr){
  var hours = (2/12)*hr;
  var canvas = document.getElementById('circle-hours');
  var context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.beginPath();
    context.arc(175, 175, 110, 0, hours*Math.PI, false);
    context.lineWidth = 40;
    context.strokeStyle = '#1b516a';
    context.stroke();
}





// ----------------------------------------
// RSS FEEDS ------------------------------
// ----------------------------------------
//all feed data and constants
var rssFeeds = [
'http://feeds.macrumors.com/MacRumors-All',
'http://www.bleedcubbieblue.com/rss/current',
'http://www.acmepackingcompany.com/rss/current',
"http://www.buckys5thquarter.com/rss/current",
'http://feeds.gawker.com/deadspin/full',
'http://feeds.gawker.com/lifehacker/full',
'http://feeds.gawker.com/io9/full',
'http://www.theverge.com/rss/group/front-page/index.xml',
'http://www.reddit.com/.rss',
'https://www.reddit.com/r/phoenix/.rss',
'http://boingboing.net/feed',
'http://news.ycombinator.com/rss',
'http://www.dvorak.org/blog/feed/',
'http://feeds.reuters.com/reuters/topNews',
"http://alistapart.com/main/feed",
"https://www.smashingmagazine.com/feed/",
"http://feeds.feedburner.com/CssTricks",
'http://www.chicagotribune.com/sports/baseball/cubs/rss2.0.xml',
];

//the total number of feeds the user has inputed
var rssTotalSize = $(rssFeeds).size();
//let's get the feeds started
var feedNumber = 0;
//let's load stuff
parseRSS(rssFeeds[0]);
//Inital load of all of the rss feeds


function parseRSS(url) {
  $.ajax({
    url: document.location.protocol + "//query.yahooapis.com/v1/public/yql?q=select%20*%20from%20xml%20where%20url%20%3D%20'" + encodeURIComponent(url) + "'&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys",
    dataType: 'json',
    success: function(data) {
      // if nothing is returned move onto the next feed
      if (data.query.results == null){
        return false;
      }
      //create a div to hold the feed
      $('.feeds').append('<div class="set fd-' + feedNumber + '"></div>');
      var container = ".fd-" + feedNumber;
      //get the feed title
      var feedTitle;
      try{ feedTitle = data.query.results.rss.channel.title; } catch(error){}
      if(feedTitle == undefined){
        feedTitle = data.query.results.feed.title;
      }
      //display the feed title
      $(container).html('<h2>'+feedTitle+'</h2>');

      //get the feed items
      var start = 0;
      var feedType;
      try{ feedType = data.query.results.rss.channel.item; } catch(error){}
      if(feedType == undefined){
        feedType = data.query.results.feed.entry;
      }
      //display the items and links
      $.each(feedType, function(key, value){
        if (start == 6){ return false; }
        var link = value.link;
        if (typeof link === 'object'){
          link = value.link.href;
        }
        var thehtml = '<h3><a href="'+link+'" target="_blank">'+value.title+'</a></h3>';
        $(container).append(thehtml);
        start++;
      });

    },
    error: function(XMLHttpRequest, textStatus, errorThrown){
      console.log('ERRRRRRROR');
    },
    complete: function(count){
     
      //next feed
      feedNumber++;
      if (feedNumber < rssTotalSize){
        //add another feed to the mix cause we are not done yet
        parseRSS( rssFeeds[feedNumber] );
      } else {
        setFeedSizes();
      }

    }
  });
}

//sets the feed boxes sizes so that they are all equal
function setFeedSizes(){
  // console.log('setting feed sizes');
  var largestHeight = 0;
  $('.set').each(function(){
    var newHeight = $(this).outerHeight();
    if (newHeight > largestHeight){
      largestHeight = newHeight;
      console.log(largestHeight);
    }
  });
  $('.set').css({'height': largestHeight});
}




//refresh the news feed
$('.refresh').on('click', function(){
    $('.feeds').html(" ");
    rssTotalSize = $(rssFeeds).size();
    feedNumber = 0;
    parseRSS(rssFeeds[0]);
});




// ----------------------------------------
// MENU -----------------------------------
// ----------------------------------------
//clicked on FEEDS
$('.menu-feeds').on('click', function(){
  
  $('.feeds').transition({'opacity': '1', delay: 300}, 300, 'ease');
  $('.refresh').css({display: 'block'}).transition({'opacity': '1', delay: 300}, 300, 'ease');

  //add the animation a classes
  $('.dashboard').addClass('container--animOutLeft');
  $('.feeds').addClass('container--animInRight');

  //lets remove those classes when this shit ends
  $('.dashboard').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',   
    function(e) {
    $('.dashboard').transition({'opacity': '0'}, 300, 'ease');
    $('.dashboard').removeClass('container--animOutLeft');
  });
  $('.feeds').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',   
    function(e) {
    $('.feeds').removeClass('container--animInRight');
  });

});

//clicked on TIME
$('.menu-time').on('click', function(){
  
  $('.dashboard').transition({'opacity': '1', delay: 300}, 300, 'ease');
  $('.refresh').transition({'opacity': '0'}, 300, 'ease').css({display: 'block'});

  $('.feeds').addClass('container--animOutRight');
  $('.dashboard').addClass('container--animInLeft');

  $('.feeds').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',   
    function(e) {
      $('.feeds').transition({'opacity': '0'}, 300, 'ease');
      $('.feeds').removeClass('container--animOutRight');
  });

  $('.dashboard').one('webkitAnimationEnd oanimationend msAnimationEnd animationend',   
    function(e) {
    $('.dashboard').removeClass('container--animInLeft');
  });

});


//clicked on LINKS

//http://tympanus.net/Tutorials/SlidingHeaderLayout/layout-multi.html



}); //end




